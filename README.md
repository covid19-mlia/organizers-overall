# Covid-19 MLIA @ Eval #

This is the overall repository of the [Covid-19 MLIA @ Eval](http://eval.covid19-mlia.eu/) community effort. 

### Organisation of the repository ###

The repository is organised as follows:

* `report`: this folder contains the rolling technical report describing the overall outcomes of the initiative, round after round.

Covid-19 MLIA @ Eval runs in *three rounds*.

### License ###

All the contents of this repository are shared using the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). 

![CC logo](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

